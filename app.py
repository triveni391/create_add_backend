from flask import Flask, request,jsonify
from models import Universities, Countrycode, Domain
import json
import re
from flask_cors import CORS, cross_origin
from mongoengine.queryset.visitor import Q
from mongoengine.errors import ValidationError
from mongoengine.errors import FieldDoesNotExist




app = Flask(__name__)
CORS(app)
def normalize_query_param(value):
    """
    Given a non-flattened query parameter value,
    and if the value is a list only containing 1 item,
    then the value is flattened.

    :param value: a value from a query parameter
    :return: a normalized query parameter value
    """
    return value if len(value) > 1 else value[0]


def normalize_query(params):
    """
    Converts query parameters from only containing one value for each parameter,
    to include parameters with multiple values as lists.

    :param params: a flask query parameters data structure
    :return: a dict of normalized query parameters
    """
    params_non_flat = params.to_dict(flat=False)
    return {k: normalize_query_param(v) for k, v in params_non_flat.items()}

@app.route('/universities',  methods=['GET', 'DELETE'])
def get():
  if request.method == 'GET':
    try: 
      print(request.get_json())
      alpha_two_code = request.args.get("alpha_two_code")
      domain = request.args.get("domain")
      pageSize = request.args.get("page_size")
      search = request.args.get("search")
      pageNumber = request.args.get("page_num")
      domain = request.args.get("domain")
      if pageNumber!=None:
        pageNumber = int(pageNumber)
      else:
        pageNumber = 0 
        offset = 0
      if pageSize!=None:
        pageSize = int(pageSize)
        offset = ((pageNumber -1) * pageSize )
      else:
        pageSize = 0  
        offset=0
      data = []
      if alpha_two_code == None and domain == None and search == None:
        return Universities.objects.skip(offset).limit(pageSize).to_json()
      if search:
        regex = re.compile(request.args.get("search"), re.IGNORECASE)
        data = Universities.objects(Q(name=regex) | Q(alpha_two_code=alpha_two_code) | Q(domain=domain)).skip(offset).limit(pageSize)
      else:  
        data = Universities.objects(Q(alpha_two_code=alpha_two_code) | Q(domain=domain)).skip(offset).limit(pageSize)
      return data.to_json() 
    except: 
      return 'Something went wrong', 500  
  else:
    try:
      result = Universities.objects(name=request.args.get("name")).delete()
      return str(result) + " document deleted."
    except: 
      "Not able to delete the data", 500 


@app.route('/update',  methods=['PUT'])
def update():
  try:
    university = Universities.objects(name=request.args.get("name"))
    body = request.get_json()
    university.update(**body)
    return 'data successfully update'
  except ValidationError as e: 
    return  str(e), 400
  except FieldDoesNotExist as e: 
    return  str(e), 400
  except:
    return 'something went wrong', 500  


@app.route('/domain')
def get_domain():
  return Domain.objects().to_json()
  

@app.route('/countrycode')
def get_university_code():
  return Countrycode.objects().to_json()


@app.route('/', methods = ["POST"])
def post():
  try:
   requestJson = request.get_json()
   data = Universities()
   data.name = requestJson["name"]
   data.country = requestJson["country"]
   data.domain = requestJson["domain"]
   data.alpha_two_code = requestJson["alpha_two_code"]
   data.web_page = requestJson["web_pages"]
   data.save()
   return "success"
  except ValidationError as e: 
    return  str(e), 400
  except: 
    return 'Something went wrong', 500  

if __name__ == "__main__":
    app.run(debug=True)
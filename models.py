from mongoengine import *

connect('universities')

class Universities(Document):
    alpha_two_code = StringField(required=True)
    country = StringField(max_length=50)
    domain = StringField(max_length=50)
    name = StringField(max_length=200)
    web_page = StringField(max_length=200)

class Domain(Document): 
    domain: StringField(required=True, unique = True)

class Countrycode(Document):
    code: StringField(required=True)
    country: StringField(required=True)
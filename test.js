const config = require('./universities.json');
const country = require('./country.json');

let result = [];

for (let index = 0; index < config.length; index++) {
  const item = config[index];
  const domain = item.domain.split(".")[1]
  if (domain === "edu" || domain === "us")
    result.push({
      ...config[index], web_pages: item.web_pages, domain
    })
}


console.log(JSON.stringify(result))